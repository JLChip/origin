var FormValidation = function () {
    // advance validation
    var handleValidation_login = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var var_form_login = $('#form_login');
            var var_error_form = $('#div_error_form', var_form_login);
            var var_error_login = $('.alert-info', var_form_login);

            

            var_form_login.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    
                    email: {
                        required: true,
                        email: true
                    },  
                    password: {
                        required: true
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    var_error_login.hide();
                    var_error_form.show();
                    Metronic.scrollTo(var_error_form, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
					/*
                    bootbox.dialog({
						title: var_mensaje_loginOK_title,
						message:var_mensaje_loginOK_message,
						buttons: {
							success: {
							  label: var_mensaje_loginOK_buttons_succes_label,
							  className: "btn-success"
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,
							  callback: function() {
								Example.show("great success");
							  }
							  *//*
							}
						}
					}); 
					*/
					//var_error_login.show();
                    var_error_form.hide();
                }

            });

            
    }
	
	
	var handleValidation_register = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var var_form_register = $('#form_register');
            var var_error_form = $('#div_error_form', var_form_register);
            var var_error_register = $('.alert-info', var_form_register);

            var_form_register.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    
                    name: {
                        required: true
                    },  
                    email: {
                        required: true,
                        email: true
                    },
					
					password: {
                        required: true, minlength: 6
                    },
					confirmarpassword: {
                         required: true, equalTo: "#password", minlength: 6
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    var_error_register.hide();
                    var_error_form.show();
                    Metronic.scrollTo(var_error_form, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
					//alert("submit");
					/*
                    bootbox.dialog({
						title: var_mensaje_loginOK_title,
						message:var_mensaje_loginOK_message,
						buttons: {
							success: {
							  label: var_mensaje_loginOK_buttons_succes_label,
							  className: "btn-success"
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,
							  callback: function() {
								Example.show("great success");
							  }
							  *//*
							}
						}
					}); 
					*/
					//var_error_login.show();
                    var_error_form.hide();
                }
				
				
				
		});
	}
	/* END REGISTER*/
			
			/*RESET*/
		 var handleValidation_reset = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var var_form_reset = $('#form_reset');
            var var_error_form = $('#div_error_form', var_form_reset);
            var var_error_reset = $('.alert-info', var_form_reset);

            

            var_form_reset.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    
                    email: {
                        required: true,
                        email: true
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    var_error_reset.hide();
                    var_error_form.show();
                    Metronic.scrollTo(var_error_form, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
					/*
                    bootbox.dialog({
						title: var_mensaje_loginOK_title,
						message:var_mensaje_loginOK_message,
						buttons: {
							success: {
							  label: var_mensaje_loginOK_buttons_succes_label,
							  className: "btn-success"
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,
							  callback: function() {
								Example.show("great success");
							  }
							  *//*
							}
						}
					}); 
					*/
					//var_error_login.show();
                    var_error_form.hide();
                }

            });

            
   		 }	

	    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {
	    	handleWysihtml5();
 	     	handleValidation_login();
		 	handleValidation_register();
			handleValidation_reset();
	    }

    };

}();