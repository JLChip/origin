/*Configuracion de las ventanas modales según METRONIC
Se hace uso de:
Bootbox
Bootbox.js is a small JavaScript library which allows you to create programmatic dialog boxes using Twitter's Bootstrap modals, without having to worry about creating, managing or removing any of the required DOM elements or JS event handlers. For more info please check out the official documentation .
*/

/*Dialogos de paginas que se componen :
loginOK -- Pagina Login
registerOK -- pagina registro
.social-icon -- Login con Facebook etc
loginFAIL -- Pagina login
resetOK -- recuperar contraseña
resetFail -- fallo en recuperar contraseña
registerFail -- Pagina registro
registerEqual -- Pagina registro
---------
eleccionIoP -- Entrada en proyecto para elegir Impresor o Printer


*/
	var UIAlertDialogApi = function () {
		var handleDialogs = function() {
			$('#loginOK').click(function(){
					bootbox.dialog({
						title: var_mensaje_loginOK_title,
						message:var_mensaje_loginOK_message,
						buttons: {
							success: {
							  label: var_mensaje_loginOK_buttons_succes_label,
							  className: "btn-success",
							   callback: function() {
								var url = url_index_logged;    
									$(location).attr('href',url);
							}
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,						  callback: function() {
								Example.show("great success");
							  }
							  */
							}
						}
					});  
			});
			
			$('#registerOK').click(function(){
					bootbox.dialog({
						title: var_mensaje_registerOK_title,
						message:var_mensaje_registerOK_message,
						buttons: {
							success: {
							  label: var_mensaje_registerOK_buttons_succes_label,
							  className: "btn-success",
							   callback: function() {
								var url = url_index_registed;    
									$(location).attr('href',url);
							}
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,						  callback: function() {
								Example.show("great success");
							  }
							  */
							}
						}
					});  
			});
			
				//end #registerOK
			$('.social-icon').click(function(){
					bootbox.dialog({
						title: var_mensaje_joiningOK_title,
						message:var_mensaje_joiningOK_message,
						buttons: {
							success: {
							  label: var_mensaje_joiningOK_buttons_succes_label,
							  className: "btn-success",
							   callback: function() {
								var url = url_index_registed;    
									$(location).attr('href',url);
							}
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,						  callback: function() {
								Example.show("great success");
							  }
							  */
							}
						}
					});  
			});
			
			$('#resetOK').click(function(){
			bootbox.dialog({
				title: var_mensaje_resetOK_title,
				message:var_mensaje_resetOK_message,
				buttons: {
					success: {
					  label: var_mensaje_resetOK_buttons_succes_label,
					  className: "btn-success",
					   callback: function() {
						var url = url_index_logged;    
							$(location).attr('href',url);
					}
					  /*Si se quiere que se llame a otra funcion al pulsar*/
					  /*,						  callback: function() {
						Example.show("great success");
					  }
					  */
					}
				}
			});  
		});
				
				
    }

    var handleAlerts = function() {
		$('#loginFAIL').click(function(){
				Metronic.alert({
               	container: $('#form_login'), // alerts parent container(by default placed after the page breadcrumbs)
                place: 'prepend', // append or prepent in container 
                type: 'info',  // alert's type
                message: var_mensaje_loginFAIL_message,  // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0,
				icon: 'warning'  // auto close after defined seconds
                
                
            });
		});
		//end #loginFAIL
		$('#registerFAIL').click(function(){
				Metronic.alert({
               	container: $('#form_register'), // alerts parent container(by default placed after the page breadcrumbs)
                place: 'prepend', // append or prepent in container 
                type: 'info',  // alert's type
                message: var_mensaje_registerFAIL_message,  // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0,
				icon: 'info'  // auto close after defined seconds
                
                
            });
		});
		//end #loginFAIL
		
		$('#registerEQUAL').click(function(){
				Metronic.alert({
               	container: $('#form_register'), // alerts parent container(by default placed after the page breadcrumbs)
                place: 'prepend', // append or prepent in container 
                type: 'warning',  // alert's type
                message: var_mensaje_registerEQUAL_message,  // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0,
				icon: 'warning'  // auto close after defined seconds
                
                
            });
		});
		//end #loginEQUAL
			
		$('#resetFAIL').click(function(){
				Metronic.alert({
               	container: $('#form_reset'), // alerts parent container(by default placed after the page breadcrumbs)
                place: 'prepend', // append or prepent in container 
                type: 'warning',  // alert's type
                message: var_mensaje_resetFAIL_message,  // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0,
				icon: 'warning'  // auto close after defined seconds
                
                
            });
		});
		
		
		  $('#eleccionIoP_boton').click(function(){
                $('#eleccionIoP').modal('show'); 
          });
         
		 $('#diseñadorSI').click(function(){
					bootbox.dialog({
						title: var_mensaje_diseñador_title,
						message:"var_mensaje_registerOK_message",
						buttons: {
							success: {
							  label: "var_mensaje_registerOK_buttons_succes_label",
							  className: "btn-success",
							   callback: function() {
								var url = url_index_registed;    
									$(location).attr('href',url);
							}
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,						  callback: function() {
								Example.show("great success");
							  }
							  */
							}
						}
					});  
			});
			$('#diseñadorNO').click(function(){
					bootbox.dialog({
						title: "var_mensaje_registerOK_title",
						message:"var_mensaje_registerOK_message",
						buttons: {
							success: {
							  label: "var_mensaje_registerOK_buttons_succes_label",
							  className: "btn-success",
							   callback: function() {
								var url = url_index_registed;    
									$(location).attr('href',url);
							}
							  /*Si se quiere que se llame a otra funcion al pulsar*/
							  /*,						  callback: function() {
								Example.show("great success");
							  }
							  */
							}
						}
					});  
			});
		 
		
		
		
		
    }
	
	

    return {

        //main function to initiate the module
        init: function () {
	        handleDialogs();
            handleAlerts();
        }
    };
	
	

}();