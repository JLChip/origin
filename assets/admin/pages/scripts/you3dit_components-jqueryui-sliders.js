var ComponentsjQueryUISliders = function () {

    return {
        //main function to initiate the module
        init: function () {
            $("#slider-distancia-range-max").slider({
                isRTL: Metronic.isRTL(),
                range: "max",
                min: 10,
                max: 1000,
                value: 10,
				 step: 10,
                slide: function (event, ui) {
                    $("#slider-distancia-range-max-amount").val(ui.value);
                }
            });
			$("#slider-precio-range-max").slider({
                isRTL: Metronic.isRTL(),
                range: "max",
                min: 0.01,
                max: 10,
                value: 0.01,
				step: 0.01,
                slide: function (event, ui) {
                    $("#slider-precio-range-max-amount").val(ui.value);
                }
            });
        }
    };
}();